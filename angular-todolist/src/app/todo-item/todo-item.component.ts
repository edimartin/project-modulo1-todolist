import { Component, OnInit, Input, HostBinding, Output, EventEmitter, Injectable } from '@angular/core';
import { callLifecycleHooksChildrenFirst } from '@angular/core/src/view/provider';
import { TodoItem } from '../../models/TodoItem.model';
import { Store } from '@ngrx/store';
import { VoteUpAction, VoteDownAction } from '../../models/todoitems-state.model';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})

@Injectable()
export class TodoItemComponent implements OnInit {
  @Input() todoItem: TodoItem;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<TodoItem>;
  constructor(public store: Store<TodoItem>) {
    this.clicked = new EventEmitter<TodoItem>();
  }

  ngOnInit() {
  }

  nextTaskClicked() {
    this.clicked.emit(this.todoItem);
    return false;
  }

  voteUp() {
    this.store.dispatch(new VoteUpAction(this.todoItem));

    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.todoItem));

    return false;
  }
}
