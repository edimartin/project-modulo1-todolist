import { TestBed, async, inject } from '@angular/core/testing';

import { UserLoggedinGuard } from './user-loggedin.guard';

describe('UserLoggedinGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserLoggedinGuard]
    });
  });

  it('should ...', inject([UserLoggedinGuard], (guard: UserLoggedinGuard) => {
    expect(guard).toBeTruthy();
  }));
});
