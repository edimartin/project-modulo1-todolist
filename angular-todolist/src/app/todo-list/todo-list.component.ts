import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TodoItem } from './../../models/TodoItem.model';
import { TodoListApiClient } from './../../models/todolist-api-client.model';
import { AppState } from '../app.module';
import { Store } from '@ngrx/store';
import { NextTaskAction, NewTodoItemAction } from './../../models/todoitems-state.model';
@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  @Output() itemAdded: EventEmitter<TodoItem>;
  ejemplos: string[];

  updates: string[];

  constructor(private todoListApiClient: TodoListApiClient, private store: Store<AppState>) {
    this.itemAdded = new EventEmitter<TodoItem>();
    this.ejemplos = ['Trabajar', 'Estudiar Angular', 'Leer un capítulo del libro'];
    this.updates = [];
    this.store.select(state => state.todoItems.nextTask)
      .subscribe(data => {
        if (data != null) {
          this.updates.push(data.titulo + ' selected');
      }
    });
  }

  ngOnInit() {
  }

  agregado(t: TodoItem) {
    this.todoListApiClient.add(t);
    this.itemAdded.emit(t);
  }

  markAsNextTask(t: TodoItem) {
    this.todoListApiClient.markAsNextTask(t);
    this.store.dispatch(new NextTaskAction(t));
  }


}
