import { Component, OnInit, EventEmitter, Output, forwardRef, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn} from '@angular/forms';
import { TodoItem } from './../../models/TodoItem.model';
import { APP_CONFIG } from 'src/models/todolist-api-client.model';
import { AppComponent } from '../app.component';
import { AppConfig } from '../app.module';
import { fromEvent } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { map, filter, switchMap, debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-form-todolist',
  templateUrl: './form-todolist.component.html',
  styleUrls: ['./form-todolist.component.css']
})
export class FormTodolistComponent implements OnInit {
  @Output() itemAdded: EventEmitter<TodoItem>;
  fg: FormGroup;
  minLength = 5;
  searchResults: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.itemAdded = new EventEmitter();

    this.fg = fb.group({
      titulo: ['', Validators.compose([
        Validators.required,
        this.validateTitleLength(this.minLength)
      ])],
      descripcion: ['']
      /*
      titulo: ['', Validators.compose([
        Validators.required,
        this.validateTitleLength(this.minLength)
      ])]
      */
    });
  }

  ngOnInit() {
  const elemTitulo = document.getElementById('titulo') as HTMLInputElement;

  fromEvent(elemTitulo, 'input')
    .pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 2),
      debounceTime(120),
      distinctUntilChanged(),
      switchMap((text: string) => ajax(this.config.apiEndPoint + '/titulos?q=' + text))
    ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);
  }

  guardar(titulo: string, descripcion: string): boolean {
    console.log('titulo:' + titulo + ', descripcion: ' + descripcion);

    const t = new TodoItem(titulo, descripcion);
    this.itemAdded.emit(t);
    return false;
  }

  validateTitleLength(minLength: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const length = control.value.toString().trim().length;

      if (length > 0 && length < minLength) {
        return { minLengthTitle: true };
      }

      return null;
    };
  }

}
