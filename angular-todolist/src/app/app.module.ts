import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, Injectable, APP_INITIALIZER } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { AppComponent } from './app.component';
import { TodoItemComponent } from './todo-item/todo-item.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { EjemploItemComponent } from './ejemplo-item/ejemplo-item.component';
import { FormTodolistComponent } from './form-todolist/form-todolist.component';
import { TodoitemDetalleComponent, TodoListApiClientDecorated } from './todoitem-detalle/todoitem-detalle.component';
import { TodoListApiClient } from './../models/todolist-api-client.model';
import { TodoItemState, reducerTodoItems, initializeTodoItemsState,
  TodoItemEffects, InitMyDataAction } from '../models/todoitems-state.model';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UserLoggedinGuard } from './guards/user-loggedin.guard';
import { AuthService } from './services/auth.service';
import { APP_CONFIG } from './../models/todolist-api-client.model';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: TodoListComponent },
  { path: 'todoItem', component: TodoitemDetalleComponent },
  { path: 'todoItem/:id', component: TodoitemDetalleComponent },
  { path: 'login', component: LoginComponent },
  { path: 'protected',
    component: ProtectedComponent,
    canActivate: [UserLoggedinGuard]
  }
];

// redux init
export interface AppState {
  todoItems: TodoItemState;
}

const reducers: ActionReducerMap<AppState> = {
  todoItems: reducerTodoItems
};

const reducersInitialState = {
  todoItems: initializeTodoItemsState()
};
// redux fin init

export interface AppConfig {
  apiEndPoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndPoint: 'http://localhost:3000'
};

// app init
export function init_app(appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.initializeTodoListState();
}

@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient) { }
  async initializeTodoListState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndPoint + '/my', '{headers: headers}' );
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}

@NgModule({
  declarations: [
    AppComponent,
    TodoItemComponent,
    TodoListComponent,
    EjemploItemComponent,
    FormTodolistComponent,
    TodoitemDetalleComponent,
    LoginComponent,
    ProtectedComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState }),
    EffectsModule.forRoot([TodoItemEffects]),
    StoreDevtoolsModule.instrument(),
    HttpClientModule
  ],
  providers: [ AuthService, UserLoggedinGuard,
    TodoListApiClient,
    {provide: TodoListApiClient, useClass: TodoListApiClientDecorated },
    AppLoadService,
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
