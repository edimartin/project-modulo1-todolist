import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ejemplo-item',
  templateUrl: './ejemplo-item.component.html',
  styleUrls: ['./ejemplo-item.component.css']
})
export class EjemploItemComponent implements OnInit {
  @Input() ejemplo: string;
  constructor() { }

  ngOnInit() {
  }

}
