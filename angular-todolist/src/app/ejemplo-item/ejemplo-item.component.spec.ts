import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EjemploItemComponent } from './ejemplo-item.component';

describe('EjemploItemComponent', () => {
  let component: EjemploItemComponent;
  let fixture: ComponentFixture<EjemploItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EjemploItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EjemploItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
