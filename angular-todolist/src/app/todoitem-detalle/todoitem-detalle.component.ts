import { Component, OnInit, Inject, forwardRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TodoListApiClient, BaseTodoListApiClient } from '../../models/todolist-api-client.model';
import { TodoItem } from '../../models/TodoItem.model';
import { APP_CONFIG } from '../../models/todolist-api-client.model';
import { AppState, AppConfig } from '../app.module';
import { Store } from '@ngrx/store';
import { HttpClient } from '@angular/common/http';


export class TodoListApiClientDecorated extends TodoListApiClient {
  constructor(
    store: Store<AppState>,
    @Inject(forwardRef(() => APP_CONFIG)) config: AppConfig,
    http: HttpClient) { super(store, config, http); }

  getById(id: string): TodoItem {
    console.log('decorated class TodoListApiClientDecorated');

    return super.getById(id);
  }
}

@Component({
  selector: 'app-todoitem-detalle',
  templateUrl: './todoitem-detalle.component.html',
  styleUrls: ['./todoitem-detalle.component.css'],
  providers: [ {provide: BaseTodoListApiClient , useExisting: TodoListApiClient} ]
})

export class TodoitemDetalleComponent implements OnInit {
  todoItem: TodoItem;
  constructor(private route: ActivatedRoute, private todoListApiClient: BaseTodoListApiClient) {}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.todoItem = this.todoListApiClient.getById(id);

    console.log('id: ' + id);
    console.log('todoItem:' + this.todoItem);
  }
}
