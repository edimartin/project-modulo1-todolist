import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoitemDetalleComponent } from './todoitem-detalle.component';

describe('TodoitemDetalleComponent', () => {
  let component: TodoitemDetalleComponent;
  let fixture: ComponentFixture<TodoitemDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoitemDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoitemDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
