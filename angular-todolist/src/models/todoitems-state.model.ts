import { TodoItem } from './TodoItem.model';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClientModule } from '@angular/common/http';


export interface TodoItemState {
    items: TodoItem[];
    loading: boolean;
    nextTask: TodoItem;
}

export const initializeTodoItemsState = function() {
    return {
        items: [],
        loading: false,
        nextTask: null
    };
};

export enum TodoItemsActionTypes {
    NEW_TODO_ITEM = '[Todo Item] Nuevo',
    NEXT_TASK = '[Todo Item] Proxima Tarea',
    VOTE_UP = '[Todo Item] Votar Up',
    VOTE_DOWN = '[Todo Item] Votar Down',
    INIT_MY_DATA = '[Todo Item] Init Data'
}

export class NewTodoItemAction implements Action {
    type = TodoItemsActionTypes.NEW_TODO_ITEM;
    constructor(public todoItem: TodoItem) {}
}

export class NextTaskAction implements Action {
    type = TodoItemsActionTypes.NEXT_TASK;
    constructor(public todoItem: TodoItem) {}
}

export class VoteUpAction implements Action {
    type = TodoItemsActionTypes.VOTE_UP;
    constructor(public todoItem: TodoItem) {}
}

export class VoteDownAction implements Action {
    type = TodoItemsActionTypes.VOTE_DOWN;
    constructor(public todoItem: TodoItem) {}
}

export class InitMyDataAction implements Action {
    type = TodoItemsActionTypes.INIT_MY_DATA;
    constructor(public todoItems: string[]) {}
}

export type TodoItemActions = NewTodoItemAction | NextTaskAction |
  VoteUpAction | VoteDownAction | InitMyDataAction;

export function reducerTodoItems(
    state: TodoItemState,
    action: TodoItemActions
): TodoItemState {
    switch (action.type) {
        case TodoItemsActionTypes.NEW_TODO_ITEM: {
            return {
                ...state,
                items: [...state.items, (action as NewTodoItemAction).todoItem]
            };
        }
        case TodoItemsActionTypes.NEXT_TASK: {
            state.items.forEach(x => x.setNextTask(false));
            const nextTaskItem: TodoItem = (action as NextTaskAction).todoItem;
            nextTaskItem.setNextTask(true);
            return {
                ...state,
                nextTask: nextTaskItem
            };
        }
        case TodoItemsActionTypes.VOTE_DOWN: {
            const t: TodoItem = (action as VoteDownAction).todoItem;
            t.voteDown();
            return {
                ...state
            };
        }
        case TodoItemsActionTypes.VOTE_UP: {
            const t: TodoItem = (action as VoteUpAction).todoItem;
            t.voteUp();
            return {
                ...state
            };
        }
        case TodoItemsActionTypes.INIT_MY_DATA: {
            const todoItems: string[] = (action as InitMyDataAction).todoItems;
            return {
                ...state,
                items: todoItems.map((t) => new TodoItem(t, ''))
            };
        }
    }

    return state;
}

@Injectable()
export class TodoItemEffects {
    @Effect()
    newAdded$: Observable<Action> = this.actions$.pipe(
        ofType(TodoItemsActionTypes.NEW_TODO_ITEM),
        map((action: NewTodoItemAction) => new NextTaskAction(action.todoItem))
    );

    constructor(private actions$: Actions) {}
}
