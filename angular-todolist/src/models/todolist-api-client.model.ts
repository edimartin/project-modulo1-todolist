import { TodoItem } from './TodoItem.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { InjectionToken, Inject, Injectable, forwardRef } from '@angular/core';
import { AppConfig, AppState } from './../app/app.module';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { NewTodoItemAction } from './todoitems-state.model';

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

export const APP_CONFIG_VALUE: AppConfig = {
    apiEndPoint: 'test.com'
  };

@Injectable()
export class BaseTodoListApiClient {
    constructor(
        ) {}
    get() {}
    post() {}
    add(t: TodoItem) {}
    getById(id: string): TodoItem {
        return null;
    }
}

@Injectable()
export class TodoListApiClient extends BaseTodoListApiClient {
    public todoItems: TodoItem[] = [];
    current: Subject<TodoItem> = new BehaviorSubject<TodoItem>(null);

    constructor(
        public store: Store<AppState>,
        @Inject(forwardRef(() => APP_CONFIG)) public config: AppConfig,
        public http: HttpClient
     ) {
        super();

        this.store
        .select(state => state.todoItems)
        .subscribe((data) => {
          console.log('todoItems sub store');
          console.log(data);
          this.todoItems = data.items;
        });
        this.store
            .subscribe((data) => {
            console.log('all store');
            console.log(data);
            });
    }

    add(t: TodoItem) {
        const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
        const req = new HttpRequest('POST', this.config.apiEndPoint + '/my',
          { titulo: t.titulo, descripcion: t.descripcion },
          {headers}
        );
        let resultOk = false;

        this.http.request(req).subscribe((data: HttpResponse<{}>) => {
            console.log('testeee');
            resultOk = true;
        });

        if (resultOk) {
            this.store.dispatch(new NewTodoItemAction(t));
        }
    }

    getTodoItems() {
        return this.todoItems;
    }

    markAsNextTask(t: TodoItem) {
        this.todoItems.forEach((x) => { x.setNextTask(false); });
        t.setNextTask(true);
        this.current.next(t);
    }

    subscribeOnChange(fn) {
        this.current.subscribe(fn);
    }

    getById(id: string) {
        console.log('filtering id:' + id);

        return this.todoItems.filter((t) => t.id.toString() === id)[0];
    }
}

