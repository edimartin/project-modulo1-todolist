import {v4 as uuid} from 'uuid';

export class TodoItem {
    titulo: string;
    descripcion: string;
    nextTask: boolean;
    id = uuid();

    constructor(titulo, descripcion: string, public votes: number = 0) {
        this.titulo = titulo;
        this.descripcion = descripcion;
    }

    isNextTask() {
        return this.nextTask;
    }

    setNextTask(value: boolean) {
      this.nextTask = value;
    }

    voteUp() {
        this.votes++;
    }

    voteDown() {
        this.votes--;
    }
}
