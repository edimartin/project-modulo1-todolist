var express = require("express"), cors = require('cors');
var app = express();
app.use(express.json());
app.use(cors());
app.listen(3000, () => console.log("Server running on port 3000"));

var tasks = ["Trabajar", "Estudiar", "Dormir temprano", "Desayuno"]
app.get("/titulos", 
(req, res, next) => {
    res.json(
        tasks.filter(
            (t) => t.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1
        )
    )
});

var myTasks = [];
app.get("/my", (req, res, next) => res.json(myTasks));
app.post("/my", (req,res,next) => {
    console.log(req.body);
    myTasks.push(req.body.titulo);
    res.json(myTasks);
});